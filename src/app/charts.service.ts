import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class ChartsService {
  url = 'assets/data.json';
  constructor(private http: HttpClient) {}

  data() {
    return this.http.get(this.url);
  }
  data1() {
    return this.http.get('http://127.0.0.1:9200/charts/_search');
  }
  data2() {
    return this.http.get('http://127.0.0.1:9200/solidgauge/_search');
  }
}
