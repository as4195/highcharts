import {Component, OnInit} from '@angular/core';
import {ChartsService} from './charts.service';
import * as Highcharts from 'highcharts';
declare var require: any;
require('highcharts/highcharts-more')(Highcharts);
require('highcharts/modules/solid-gauge')(Highcharts);
require('highcharts/modules/stock')(Highcharts);

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  highcharts = Highcharts;
  chartOptions: any;
  charts: any;
  chartHolder;
  solidGauge: any;

  constructor(private chartS: ChartsService) {
  }

  ngOnInit() {
    this.chartS.data1().subscribe((data) => {
      this.charts = data;
      this.chartOptions = this.charts.hits.hits;
    });
    this.chartS.data2().subscribe((data) => {
      this.charts = data;
      this.solidGauge = this.charts.hits.hits;
      this.chartHolder = Highcharts.chart('container-speed', this.solidGauge[0]._source);
    });
    setInterval(() => {
      // Speed
      var point,
        newVal,
        inc;

      if (this.solidGauge[0]._source) {
        point = this.solidGauge[0]._source.series[0].data[0];
        inc = Math.round((Math.random() - 0.5) * 100);
        newVal = point + inc;

        if (newVal < 0 || newVal > 200) {
          newVal = point - inc;
        }
        this.solidGauge[0]._source.series[0].data[0] = newVal;
        this.chartHolder = Highcharts.chart('container-speed', this.solidGauge[0]._source);
      }
    }, 2000);
    Highcharts.chart('container', {
      chart: {
      },

      time: {
        useUTC: false
      },

      rangeSelector: {
        buttons: [{
          count: 1,
          type: 'minute',
          text: '1M'
        }, {
          count: 5,
          type: 'minute',
          text: '5M'
        }, {
          type: 'all',
          text: 'All'
        }],
        inputEnabled: false,
        selected: 0
      },

      title: {
        text: 'Live random data'
      },
      exporting: {
        enabled: false
      },
      series: [{name: 'Random data',
      data: [80,90,100,110,120,130,140,130,120,110,40,80]}]
  });
    this.chartS.data().subscribe((data) => {
      const view = data;
      const months = {
        1: 'January',
        2: 'Feburary',
        3: 'March',
        4: 'April',
        5: 'May',
        6: 'June',
        7: 'July',
        8: 'August',
        9: 'September',
        10: 'October',
        11: 'November',
        12: 'December'
      }
      const buckets = view[0].aggregations.periods.buckets;
      var result = buckets.map((dat) => {
        var values = dat.all_opps.buckets;
        var send = [{name: 'WON' , value : 0 }, {name: 'SUBMITTED' , value : 0 }, {name: 'LOST' , value : 0 }]
        if (values.length) {
          const date = new Date(dat.from_as_string);
          values.map((da) => {
           if (da.key === 'WON') {
             send[0].value = da.doc_count;
           } else if (da.key === 'SUBMITTED') {
             send[1].value = da.doc_count;
           } else if (da.key === 'LOST') {
             send[2].value = da.doc_count;
           } else {
             return null;
           }
          });
          const filtered = send.filter((el) => {
            return el != null;
          });
          return {category : months[date.getDate()] + date.getFullYear(),
            values: filtered};
        } else {
          const date = new Date(dat.from_as_string);
          return {category : months[date.getDate()] + date.getFullYear(),
            values: send};
        }
      });
    });
  }
}
