This Project needs data that needs to provided through elastic search with the name charts and solidgauge and you can insert the data using curl query.

curl -H "Content-Type: application/json" -XPOST "localhost:9200/charts/_bulk?pretty&refresh" --data-binary "@chart.json"
curl -H "Content-Type: application/json" -XPOST "localhost:9200/charts/_bulk?pretty&refresh" --data-binary "@solidgauge.json"

These JSON File will be present in output folder inside source

Too access elastic in angular you need to do specific changes in elasticsearch.yml file
which you can find in elasticsearch\config

These to lines need to be added to enable CORS for elasticsearch to be accessed

http.cors.allow-origin: "/.*/"
http.cors.enabled: true

These changes screenshot in output folder

Too see if both are uploaded on elastic search than you can use this command

curl "localhost:9200/_cat/indices?v"

later you can do 

npm i

ng serve 

Results will be displayed as Shown in output folder and you can always try new stuff feel free to share your ideas of making the code better
